# -*- coding: utf-8 -*-
"""
Created on Fri May 17 19:30:49 2024

@author: marco
"""


# i want to give an option to play the game or not, however I
# think I will add this for the final
import sys

playername = input("Please enter your name: ")

print(f"Hello {playername}!")

# first choice for the player, collecting the name
start = input((f"{playername} Would you like to play a game? YES or NO? "))
if start == ("YES").lower().strip():
    print("\nOK, we shall begin...")
    print(f"{playername} to play the game, the action prompts will be in")
    print("uppercase. Just type those in to progress through the game")
    print("OK, that's it! And away we go!!!!")
else:
    print("Ok, thanks anyways!")
    input("Press enter to exit")
    sys.exit()
# game start
# first state outside the hospital
print("\nFor some reason, you like to poke around really creepy places")
print("\nTonight, you chose to check out the abandoned psychiatric hospital")
first_choice = input("\nDo you GO IN or GO HOME? ")
if first_choice == ("GO IN").lower().strip():
    print("\nYou turn on your flashlight and walk inside")
elif first_choice == ("GO HOME").lower().strip():
    print("You turn around and go home")
    print("You hear a hiss behind you...you turn around and everything")
    print("goes black")
    input("\nYou have died. Press enter to exit")
    sys.exit()
else:
    print("You hear a hiss behind you...you turn around and everything")
    print("goes black")
    input("\nYou have died. Press enter to exit")
    sys.exit()

# second room
# main lobby

print("\nYou step into the main entrance of the hospital")
print("you see a door on your left and right, and another door")
print("at the end of the hallway")

beginning = ("\nYou are now in the main lobby")
print(beginning)
main_lobby = input("\nDo you go LEFT, RIGHT, or STRAIGHT? ")
# third room
# janitor closet
if main_lobby == ("LEFT").lower().strip():
    print("\nYou turn left and walk to the door. It says 'Janitor Closet'")
    print("\nYou hear angry screeching noises coming from inside")
    door_choice_one = input("\nDo you OPEN the door or GO BACK to the lobby? ")
    if door_choice_one == ("OPEN").lower().strip():
        print("\nYou open the door...a angry racoon hisses and rushes you!")
        closetanimal = input("Do you FIGHT the racoon? Or do you RUN?")
        if closetanimal == ("FIGHT").lower().strip():
            print("The racoon mauls your eyeballs! Oh no!")
            input("\nYou have died. Press enter to exit")
        else:
            print("\nYou hear angry hissing as you scurry back to the lobby")
            print("The hissing is getting closer!")
            print("The racoon jumps on your back and mauls your neck! Oh no!")
            input("\nYou have died. Press enter to exit")
            sys.exit()
    elif door_choice_one == ("GO BACK").lower().strip():
        print("\nYou hear angry hissing as you scurry back to the lobby")
        print("The hissing is getting closer!")
        print("The racoon jumps on your back and mauls your neck! Oh no!")
        input("\nYou have died. Press enter to exit")
        sys.exit()
# fourth room to explore
# pharmacy
elif main_lobby == ("RIGHT").lower().strip():
    print("You walk up the the door labled 'Pharmacy'")
    print("The door looks like it is cracked open slightly.")
    print("It looks like you can squeeze in")
    pharmacydoor = input("Do you try and GO IN or LEAVE?")
    if pharmacydoor == ("GO IN").lower().strip():
        print("You squeeze in....and get stuck")
        print("You might be able to push hard and go inside the pharmacy")
        tightdoor = input("Do you TRY AGAIN or GO BACK? ")
        if tightdoor == ("TRY AGAIN").lower().strip():
            print("You manage to stumble into the pharmacy...")
            print("...as you drop your flashlight onto the main lobby...")
            print("and of course this happens as the door closes")
        elif tightdoor == ("GO BACK").lower().strip():
            print("\nYou manage to push freeof the door, but it pinches your")
            print("foot as you come out. You hear a loud bang and the door")
            print("slams against your door, pinching it off")
            print("You try your cell phone, but there is no service")
            print("\nLooks like you might be here a while...")
            print("\nYou're so cold and tired...you decide to close your eyes")
            input("\nYou have died. PRess enter to exit")
            sys.exit()
# fifth room
# hallway

elif main_lobby == ("STRAIGHT").lower().strip():
    print("\nYou walk down the hallway")
    print("As you walk down, it seems like the door at the end of the hallway")
    print("Is getting further and further away")
    hallway = input("\nDo you KEEP walking or TURN around? ")
    if hallway == input("KEEP").lower().strip():
        print("\nYou keep walking...and walking...eventually everything")
        print("turns black...you can't hear, see, smell...")
        input("\nYou have died. Press enter to exit")
        sys.exit()
    else:
        print("\nYou start walking back to the main lobby")
        print("The hallway keeps stretching and stretching....no matter how")
        print("fast you run, you keep running and running")
        print("You keep running...and running..and running..for all eternity")
        input("\nYou have died. Press enter to exit")
        sys.exit()
