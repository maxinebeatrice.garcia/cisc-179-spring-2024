def readText(section):
    p=False
    text=""
    Tstart=("&"+section)
    Tend=("#"+section)
    
    file_path=r"C:\Users\lucap\OneDrive\Desktop\patapoutian_final\patapoutian.txt"
    with open(file_path,"r") as file:
        descriptions=file.readlines()
    for line in descriptions:
        if Tend in line:
            p=False
        if(p):
            text+=line
        if Tstart in line:
            p=True
    return text
#random to be used because making the right choice doesn't always mean
#reaching the right result in baseball
import random
#Defining different situations
main_txt=readText("main")
tutorial_txt=readText("tutorial")
spring_training_txt=readText("spring_training")
prospect_txt=readText("prospect")
trade_txt=readText("trade")
injury_txt=readText("injury")
wild_card_txt=readText("wild_card")
division_series_txt=readText("division_series")
championship_series_txt=readText("championship_series")
world_series_txt=readText("world_series")
endscreen_txt=readText("endscreen")
txt_005=readText("005")
txt_000=readText("000")
txt_11a=readText("11a")
txt_10a=readText("10a")
txt_11b=readText("11b")
txt_10b=readText("10b")
txt_21a=readText("21a")
txt_22a=readText("22a")
txt_20b=readText("20b")
txt_21b=readText("21b")
txt_20c=readText("20c")
txt_21c=readText("21c")
txt_31a=readText("31a")
txt_32a=readText("32a")
txt_33a=readText("33a")
txt_30b=readText("30b")
txt_31b=readText("31b")
txt_32b=readText("32b")
txt_31c=readText("31c")
txt_32c=readText("32c")
txt_33c=readText("33c")
txt_30d=readText("30d")
txt_31d=readText("31d")
txt_32d=readText("32d")
secret0_1_txt=readText("secret0_1")
secret0_2_txt=readText("secret0_2")
secret1_txt=readText("secret1")
txt_41a=readText("41a")
txt_42a=readText("42a")
txt_43a=readText("43a")
txt_44a=readText("44a")
txt_40b=readText("40b")
txt_41b=readText("41b")
txt_42b=readText("42b")
txt_43b=readText("43b")
txt_41c=readText("41c")
txt_42c=readText("42c")
txt_43c=readText("43c")
txt_44c=readText("44c")
txt_40d=readText("40d")
txt_41d=readText("41d")
txt_42d=readText("42d")
txt_43d=readText("43d")
txt_wc_w1=readText("wc_w1")
txt_wc_l1=readText("wc_l1")
txt_wc_w2=readText("wc_w2")
txt_wc_l2=readText("wc_l2")
txt_ds_w1=readText("ds_w1")
txt_ds_l1=readText("ds_l1")
txt_ds_w2=readText("ds_w2")
txt_ds_l2=readText("ds_l2")
txt_ws_w1=readText("ws_w1")
txt_ws_l1=readText("ws_l1")
txt_ws_w2=readText("ws_w2")
txt_ws_l2=readText("ws_l2")
txt_cs_w1=readText("cs_w1")
txt_cs_l1=readText("cs_l1")
txt_cs_w2=readText("cs_w2")
txt_cs_l2=readText("cs_l2")
#Match counter and win counter to keep track of place in season and if the
#team should advance, b is bonus for choice in tutorial
m=-2
w=0
b=0
a=1
#defining States/Rooms
def state(m,w,b,a):
    if m==-2:
        main(m,w,b,a)
    elif m==-1:
        tutorial(m,w,b,a)
    elif m==0:
        spring_training(m,w,b,a)
    elif m==1:
        prospect(m,w,b,a)
    elif m==2:
        trade(m,w,b,a)
    elif m==3:
        injury(m,w,b,a)
    elif m==4:
        wild_card(m,w,b,a)
    elif m==5:
        division_series(m,w,b,a)
    elif m==6:
        championship_series(m,w,b,a)
    elif m==7:
        world_series(m,w,b,a)
    elif m==8:
        endscreen(m,w,b,a)
#coding rooms
def main(m,w,b,a):#start screen, want to preserve variables m, w and b
    print(main_txt)
    tutorial_choice=input("Type your choice here: ")#get user input
    tutorial_choice=tutorial_choice.lower().strip()#remove caps and spaces
    if tutorial_choice=="y" or tutorial_choice=="yes": #handling different inputs
        m=-1#pathfinding
        state(m, w, b, a)#continuing the game
    elif tutorial_choice=="n" or tutorial_choice=="no":
        m=0
        state(m, w, b, a)
    elif "y" in tutorial_choice:
        confirm=input("Did you mean Yes? If so please type either 'y' or 'yes':")
        confirm=confirm.lower().strip()
        if confirm=="y" or confirm=='yes':
            m=-1
            state(m, w, b, a)
        else:
            print("Sorry for the misunderstanding: lets try that again \n")
            state(m, w, b, a)
    elif "n" in tutorial_choice:
        confirm=input("Did you mean No? If so please type either 'n' or 'no':")
        confirm=confirm.lower().strip()
        if confirm=="n" or confirm=="no":
            m=0
            state(m, w, b, a)
        else:
            print("Sorry for the misunderstanding: lets try that again")
            state(m, w, b, a)
    else:
        print("That wasn't close to either of those options. Please try again.")
        state(m,w,b,a)
def tutorial(m,w,b,a):#tutorial, still keeping m w and b
    print(tutorial_txt)
    manager_choice=input("Type your choice here: ")
    manager_choice=manager_choice.lower().strip()
    if manager_choice=="g" or manager_choice=="griggs" or manager_choice=="w" or manager_choice=="wiley" or manager_choice=="wg" or manager_choice=="wileygriggs":
        m=0
        b=5
        print(txt_005)
        state(m, w, b, a)
    elif manager_choice=="m" or manager_choice=="mejia" or manager_choice=="miguel" or manager_choice=="miguelmejia" or manager_choice=="mm":
        m=0
        print(txt_000)
        state(m, w, b, a)
    elif "g" in manager_choice:
        confirm=input("Did you mean Griggs? If so please type either 'g' or 'griggs':")
        confirm=confirm.lower().strip()
        if confirm=="g" or confirm=="griggs":
            m=0
            b=5 
            print(txt_005)
            state(m, w, b, a)
        else:
            print("Sorry for the misunderstanding: lets try that again")
            state(m, w, b, a)
    elif "m" in manager_choice:
        confirm=input("Did you mean Mejia? If so please type either 'm' or 'mejia':")
        confirm=confirm.lower().strip()
        if confirm=="m" or confirm=="mejia":
            m=0
            print(txt_000)
            state(m, w, b, a)
        else:
            print("Sorry for the misunderstanding: lets try that again")
            state(m, w, b, a)
    else:
        print("That wasn't close to either of those options. Please try again.")
        state(m,w,b,a)
def spring_training(m,w,b,a):#game choice one
        x=random.randrange(1,101)#randomness :3
        print(spring_training_txt)
        spring_training_choice=input("Type your choice here: ")
        spring_training_choice=spring_training_choice.lower().strip()
        if spring_training_choice=="s" or spring_training_choice=="shane" or spring_training_choice=="r" or spring_training_choice=="reynolds" or spring_training_choice=="sr" or spring_training_choice=="shanereynolds":
            if x+b>40:
                w=w+1
                m=1
                print(txt_11a)
                state(m, w, b, a)
            else:
                m=1
                print(txt_10a)
                state(m, w, b, a)
        elif spring_training_choice=="c" or spring_training_choice=="curley" or spring_training_choice=="w" or spring_training_choice=="williams" or spring_training_choice=="cw" or spring_training_choice=="curleywilliams":
            if x+b>80:
                w=w+1
                m=1
                print(txt_11b)
                state(m, w, b, a)
            else:
                m=1
                print(txt_10b)
                state(m, w, b, a)
        elif "s" in spring_training_choice or "r" in spring_training_choice:
            confirm=input("Did you mean Shane Reynolds? If so please type either 'S' or 'Shane':")
            confirm=confirm.lower().strip()
            if confirm=="s" or confirm=="shane":
                if x+b>40:
                    w=w+1
                    m=1
                    print(txt_11a)
                    state(m, w, b, a)
                else:
                    m=1
                    print(txt_10a)
                    state(m, w, b, a)
            else:
                print("Sorry for the misunderstanding: lets try that again")
                state(m, w, b, a)
        elif "w" in spring_training_choice or "c" in spring_training_choice:
            confirm=input("Did you mean Curley Williams? If so please type either 'C' or 'Curley':")
            confirm=confirm.lower().strip()
            if confirm=="c" or confirm=="curley":
               if x+b>80:
                   w=w+1
                   m=1
                   print(txt_11b)
                   state(m, w, b, a)
               else:
                   m=1
                   print(txt_10b)
                   state(m, w, b, a)
            else:
                print("Sorry for the misunderstanding: lets try that again")
                state(m, w, b, a)
        else:
            print("That wasn't close to either of those options. Please try again.")
            state(m,w,b,a)
def prospect(m,w,b,a):#game choice two
        x=random.randrange(1,101)
        print(prospect_txt)
        prospect_choice=input("Type your choice here: ")
        prospect_choice=prospect_choice.lower().strip()
        if prospect_choice=="y" or prospect_choice=="yes":
            a=1
            if x+m>20:
                w=w+1
                m=2
                if w==1:
                    print(txt_21a)
                    state(m,b,w,a)
                else:
                    print(txt_22a)
                    state(m, w, b,a)   
            else:
                m=2
                if w==0:
                    print(txt_20b)
                    state(m, w, b,a)
                else:
                    print(txt_21b)
                    state(m,w,b,a)
        elif prospect_choice=="n" or prospect_choice=="no":
            a=0
            m=2
            if w==0:
                print(txt_20c)
                state(m,b,w,a)
            else:
                print(txt_21c)
                state(m, w, b,a)  
        elif "y" in prospect_choice:
            confirm=input("Did you mean Yes? If so please type either 'Y' or 'Yes':")
            confirm=confirm.lower().strip()
            if confirm=="y" or confirm=="yes":
                a=1
                if x+m>20:
                    w=w+1
                    m=2
                    if w==1:
                        print(txt_21a)
                        state(m,b,w,a)
                    else:
                        print(txt_22a)
                        state(m, w, b,a)   
                else:
                    m=2
                    if w==0:
                        print(txt_20b)
                        state(m, w, b,a)
                    else:
                        print(txt_21b)
                        state(m,w,b,a)
            else:
                print("Sorry for the misunderstanding: lets try that again")
                state(m, w, b, a)
        elif "n" in prospect_choice:
            confirm=input("Did you mean No? If so please type either 'N' or 'No':")
            confirm=confirm.lower().strip()
            if confirm=="n" or confirm=="no":
               a=0
               m=2
               if w==0:
                   print(txt_20c)
                   state(m,b,w,a)
               else:
                   print(txt_21c)
                   state(m, w, b,a)  
            else:
                print("Sorry for the misunderstanding: lets try that again")
                state(m, w, b, a)
        else:
            print("That wasn't close to either of those options. Please try again.")
            state(m,w,b,a)
def trade(m,w,b,a):#game choice three
        x=random.randrange(1,101)
        print(trade_txt)
        trade_choice=input("Type your choice here: ")
        trade_choice=trade_choice.lower().strip()
        if trade_choice=="t" or trade_choice=="terry" or trade_choice=="c" or trade_choice=="cox" or trade_choice=="tc" or trade_choice=="terrycox":
            if x+b>75:
                w=w+1
                m=3
                if w==1:
                    print(txt_31a)
                    state(m, w, b, a)
                elif w==2:
                    print(txt_32a)
                    state(m, w, b, a)
                else:
                    print(txt_33a)
                    state(m, w, b, a)   
            else:
                m=3
                if w==0:
                    print(txt_30b)
                    state(m, w, b, a)
                elif w==1:
                    print(txt_31b)
                    state(m, w, b, a)
                else:
                    print(txt_32b)
                    state(m, w, b, a)
        elif trade_choice=="f" or trade_choice=="fain" or trade_choice=="ferris" or trade_choice=="ff" or trade_choice=="ferrisfain":
            if x+b>25:
                w=w+1
                m=3
                if w==1:
                    print(txt_31c)
                    state(m, w, b, a)
                elif w==2:
                    print(txt_32c)
                    state(m, w, b, a)
                else:
                    print(txt_33c)
                    state(m, w, b, a)  
            else:
                m=3
                if w==0:
                    print(txt_30d)
                    state(m, w, b, a)
                elif w==1:
                    print(txt_31d)
                    state(m, w, b, a)
                else:
                    print(txt_32d)
                    state(m, w, b, a)
        elif trade_choice=="both":
            if a==0:
                w=w+1
                m=3
                if w==1:
                    print(secret0_1_txt)
                    state(m, w, b, a)
                else:
                    print(secret0_2_txt)
                    state(m, w, b, a)
            else:
                print(secret1_txt)
                state(m,w,b,a)
        elif "t" in trade_choice or"c" in trade_choice:
            confirm=input("Did you mean Terry Cox? If so please type either 'T' or 'Terry':")
            confirm=confirm.lower().strip()
            if confirm=="t" or confirm=="terry":
                if x+b>75:
                    w=w+1
                    m=3
                    if w==1:
                        print(txt_31a)
                        state(m, w, b, a)
                    elif w==2:
                        print(txt_32a)
                        state(m, w, b, a)
                    else:
                        print(txt_33a)
                        state(m, w, b, a)   
                else:
                    m=3
                    if w==0:
                        print(txt_30b)
                        state(m, w, b, a)
                    elif w==1:
                        print(txt_31b)
                        state(m, w, b, a)
                    else:
                        print(txt_32b)
                        state(m, w, b, a)
            else:
                print("Sorry for the misunderstanding: lets try that again")
                state(m, w, b, a)
        elif "f" in trade_choice:
            confirm=input("Did you mean Ferris Fain? If so please type either 'F' or 'Ferris':")
            confirm=confirm.lower().strip()
            if confirm=="f" or confirm=="ferris":
               if x+b>25:
                   w=w+1
                   m=3
                   if w==1:
                       print(txt_31c)
                       state(m, w, b, a)
                   elif w==2:
                       print(txt_32c)
                   else:
                       print(txt_33c)
                       state(m, w, b, a)  
               else:
                   m=3
                   if w==0:
                       print(txt_30d)
                       state(m, w, b, a)
                   elif w==1:
                       print(txt_31d)
                       state(m, w, b, a)
                   else:
                       print(txt_32d)
                       state(m, w, b, a)
            else:
                print("Sorry for the misunderstanding: lets try that again")
                state(m, w, b, a)
        else:
            print("That wasn't close to either of those options. Please try again.")
            state(m,w,b,a)
def injury(m,w,b,a):#game choice four
        x=random.randrange(1,101)
        print(injury_txt)
        injury_choice=input("Type your choice here: ")
        injury_choice=injury_choice.lower().strip()
        if injury_choice=="m" or injury_choice=="marv" or injury_choice=="g" or injury_choice=="grissom" or injury_choice=="mg" or injury_choice=="marvgrissom":
            if x+b>15:
                w=w+1
                if w==1:
                    print(txt_41a)
                    m=8
                    state(m, w, b, a)
                elif w==2:
                    print(txt_42a)
                    m=4
                    state(m, w, b, a)
                elif w==3:
                    print(txt_43a)
                    m=4
                    state(m, w, b, a)
                else:
                    print(txt_44a)
                    m=5
                    state(m, w, b, a)   
            else:
                if w==0:
                    print(txt_40b)
                    m=8
                    state(m, w, b, a)
                elif w==1:
                    print(txt_41b)
                    m=8
                    state(m, w, b, a)
                elif w==2:
                    print(txt_42b)
                    m=4
                    state(m, w, b, a)
                else:
                    print(txt_43b)
                    m=4
                    state(m, w, b, a)
        elif injury_choice=="d" or injury_choice=="darrell" or injury_choice=="e" or injury_choice=="evans" or injury_choice=="de" or injury_choice=="darrellevans":
            if x+b>50:
                w=w+1
                if w==1:
                    print(txt_41c)
                    m=8
                    state(m, w, b, a)
                elif w==2:
                    print(txt_42c)
                    m=4
                    state(m, w, b, a)
                elif w==3:
                    print(txt_43c)
                    m=4
                    state(m, w, b, a)
                else:
                    print(txt_44c)
                    m=5
                    state(m, w, b, a)  
            else:
                if w==0:
                    print(txt_40d)
                    m=8
                    state(m, w, b, a)
                elif w==1:
                    print(txt_41d)
                    m=8
                    state(m, w, b, a)
                elif w==2:
                    print(txt_42d)
                    m=4
                    state(m, w, b, a)
                else:
                    print(txt_43d)
                    m=4
                    state(m, w, b, a)
        elif "m" in injury_choice or "g" in injury_choice:
            confirm=input("Did you mean Marv Grissom? If so please type either 'M' or 'Marv':")
            confirm=confirm.lower().strip()
            if confirm=="m" or confirm=="marv":
                if x+b>15:
                    w=w+1
                    if w==1:
                        print(txt_41a)
                        m=8
                        state(m, w, b, a)
                    elif w==2:
                        print(txt_42a)
                        m=4
                        state(m, w, b, a)
                    elif w==3:
                        print(txt_43a)
                        m=4
                        state(m, w, b, a)
                    else:
                        print(txt_44a)
                        m=5
                        state(m, w, b, a)   
                else:
                    if w==0:
                        print(txt_40b)
                        m=8
                        state(m, w, b, a)
                    elif w==1:
                        print(txt_41b)
                        m=8
                        state(m, w, b, a)
                    elif w==2:
                        print(txt_42b)
                        m=4
                        state(m, w, b, a)
                    else:
                        print(txt_43b)
                        m=4
                        state(m, w, b, a)
            else:
                print("Sorry for the misunderstanding: lets try that again")
                state(m, w, b, a)
        elif "d" in injury_choice or "e" in injury_choice:
            confirm=input("Did you mean Darrell Evans? If so please type either 'D' or 'Darrell':")
            confirm=confirm.lower().strip()
            if confirm=="d" or confirm=="darrell":
               if x+b>50:
                   w=w+1
                   if w==1:
                       print(txt_41c)
                       m=8
                       state(m, w, b, a)
                   elif w==2:
                       print(txt_42c)
                       m=4
                       state(m, w, b, a)
                   elif w==3:
                       print(txt_43c)
                       m=4
                       state(m, w, b, a)
                   else:
                       print(txt_44c)
                       m=5
                       state(m, w, b, a)  
               else:
                   if w==0:
                       print(txt_40d)
                       m=8
                       state(m, w, b, a)
                   elif w==1:
                       print(txt_41d)
                       m=8
                       state(m, w, b, a)
                   elif w==2:
                       print(txt_42d)
                       m=4
                       state(m, w, b, a)
                   else:
                       print(txt_43d)
                       m=4
                       state(m, w, b, a)
            else:
                print("Sorry for the misunderstanding: lets try that again")
                state(m, w, b, a)
        else:
            print("That wasn't close to either of those options. Please try again.")
            state(m,w,b,a)
def wild_card(m,w,b,a):#playoff choice 1
    x=random.randrange(1,101)
    print(wild_card_txt)
    wild_card_choice=input("Type your choice here: ")
    wild_card_choice=wild_card_choice.strip().lower()
    if wild_card_choice=="bp" or wild_card_choice=="b" or wild_card_choice=="bullpen":
        if x+b>50:
            m=5
            print(txt_wc_w1)
            state(m, w, b, a)
        else:
            m=8
            print(txt_wc_l1)
            state(m, w, b, a)
    elif wild_card_choice=="starter" or wild_card_choice=="sp" or wild_card_choice=="s":
        if x+b>25:
            m=5
            print(txt_wc_w2)
            state(m, w, b, a)
        else:
            m=8
            print(txt_wc_l2)
            state(m, w, b, a)
    elif "b" in wild_card_choice:
        confirm=input("Did you mean Bullpen? If so please type either 'B' or 'Bullpen': ")
        confirm=confirm.lower().strip()
        if confirm=="b" or confirm=="bullpen":
            if x+b>50:
                m=5
                print(txt_wc_w1)
                state(m, w, b, a)
            else:
                m=8
                print(txt_wc_l1)
                state(m, w, b, a)
        else:
            print("Sorry for the misunderstanding: lets try that again")
            state(m, w, b, a)
    elif "s" in wild_card_choice:
        confirm=input("Did you mean Starter? If so please type either 'S' or 'Starter': ")
        confirm=confirm.lower().strip()
        if confirm=="s" or confirm=="starter":
            if x+b>25:
                m=5
                print(txt_wc_w2)
                state(m, w, b, a)
            else:
                m=8
                print(txt_wc_l2)
                state(m, w, b, a)
        else:
            print("Sorry for the misunderstanding: lets try that again")
            state(m, w, b, a)
    else:
        print("That wasn't close to either of those options. Please try again.")
        state(m,w,b,a)
def division_series(m,w,b,a):#playoff choice 2
    x=random.randrange(1,101)
    print(division_series_txt)
    division_series_choice=input("Type your choice here: ")
    division_series_choice=division_series_choice.strip().lower()
    if division_series_choice=="stay" or division_series_choice=="s":
        if x+b>75:
            m=6
            print(txt_ds_w1)
            state(m, w, b, a)
        else:
            m=8
            print(txt_ds_l1)
            state(m, w, b, a)
    elif division_series_choice=="mix" or division_series_choice=="m":
        if x+b>30:
            m=6
            print(txt_ds_w2)
            state(m, w, b, a)
        else:
            m=8
            print(txt_ds_l2)
            state(m, w, b, a)
    elif "s" in division_series_choice:
        confirm=input("Did you mean Stay? If so please type either 'S' or 'Stay': ")
        confirm=confirm.lower().strip()
        if confirm=="s" or confirm=="stay":
            if x+b>75:
                m=6
                print(txt_ds_w1)
                state(m, w, b, a)
            else:
                m=8
                print(txt_ds_l1)
                state(m, w, b, a)
        else:
            print("Sorry for the misunderstanding: lets try that again")
            state(m, w, b, a)
    elif "m" in division_series_choice:
        confirm=input("Did you mean Mix? If so please type either 'M' or 'Mix': ")
        confirm=confirm.lower().strip()
        if confirm=="m" or confirm=="mix":
            if x+b>30:
                m=6
                print(txt_ds_w2)
                state(m, w, b, a)
            else:
                m=8
                print(txt_ds_l2)
                state(m, w, b, a)
        else:
            print("Sorry for the misunderstanding: lets try that again")
            state(m, w, b, a)
    else:
        print("That wasn't close to either of those options. Please try again.")
        state(m,w,b,a)
def championship_series(m,w,b,a):#playoff choice 3
    x=random.randrange(1,101)
    print(championship_series_txt)
    championship_series_choice=input("Type your choice here: ")
    championship_series_choice=championship_series_choice.strip().lower()
    if championship_series_choice=="8" or championship_series_choice=="eight" or championship_series_choice=="eighth" or championship_series_choice=="8th":
        if x+b>20:
            m=7
            print(txt_cs_w1)
            state(m, w, b, a)
        else:
            m=8
            print(txt_cs_l1)
            state(m, w, b, a)
    elif championship_series_choice=="9" or championship_series_choice=="nine" or championship_series_choice=="9th" or championship_series_choice=="ninth":
        if x+b>50:
            m=7
            print(txt_cs_w2)
            state(m, w, b, a)
        else:
            m=8
            print(txt_cs_l2)
            state(m, w, b, a)
    elif "e" in championship_series_choice or "8" in championship_series_choice:
        confirm=input("Did you mean the 8th? If so please type either '8' or 'eight': ")
        confirm=confirm.lower().strip()
        if confirm=="eight" or confirm=="8":
            if x+b>20:
                m=7
                print(txt_cs_w1)
                state(m, w, b, a)
            else:
                m=8
                print(txt_cs_l1)
                state(m, w, b, a)
        else:
            print("Sorry for the misunderstanding: lets try that again")
            state(m, w, b, a)
    elif "n" in championship_series_choice or "9" in championship_series_choice:
        confirm=input("Did you mean the 9th? If so please type either '9' or 'nine': ")
        confirm=confirm.lower().strip()
        if confirm=="9" or confirm=="nine":
            if x+b>50:
                m=7
                print(txt_cs_w2)
                state(m, w, b, a)
            else:
                m=8
                print(txt_cs_l2)
                state(m, w, b, a)
        else:
            print("Sorry for the misunderstanding: lets try that again")
            state(m, w, b, a)
    else:
        print("That wasn't close to either of those options. Please try again.")
        state(m,w,b,a)
def world_series(m,w,b,a):#playoff choice 4
    x=random.randrange(1,101)
    print(world_series_txt)
    world_series_choice=input("Type your choice here: ")
    world_series_choice=world_series_choice.strip().lower()
    if world_series_choice=="bullpen" or world_series_choice=="bp" or world_series_choice=="b":
        if x+b>80:
            m=8
            print(txt_ws_w1)
            state(m, w, b, a)
        else:
            m=8
            print(txt_ws_l1)
            state(m, w, b, a)
    elif world_series_choice=="starter" or world_series_choice=="starting pitcher" or world_series_choice=="sp" or world_series_choice=="s":
        if x+b>30:
            m=8
            print(txt_ws_w2)
            state(m, w, b, a)
        else:
            m=8
            print(txt_ws_l2)
            state(m, w, b, a)
    elif "b" in world_series_choice:
        confirm=input("Did you mean bullpen? If so please type either 'bp' or 'bullpen': ")
        confirm=confirm.lower().strip()
        if confirm=="bp" or confirm=="bullpen":
            if x+b>20:
                m=8
                print(txt_ws_w1)
                state(m, w, b, a)
            else:
                m=8
                print(txt_ws_l1)
                state(m, w, b, a)
        else:
            print("Sorry for the misunderstanding: lets try that again")
            state(m, w, b, a)
    elif "s" in world_series_choice:
        confirm=input("Did you mean starting pitcher? If so please type either 's' or 'starter': ")
        confirm=confirm.lower().strip()
        if confirm=="s" or confirm=="starter":
            if x+b>50:
                m=8
                print(txt_ws_w2)
                state(m, w, b, a)
            else:
                m=8
                print(txt_ws_l2)
                state(m, w, b, a)
        else:
            print("Sorry for the misunderstanding: lets try that again")
            state(m, w, b, a)
    else:
        print("That wasn't close to either of those options. Please try again.")
        state(m,w,b,a)
def endscreen(m,b,w,a):
    print(endscreen_txt)
    endscreen_choice=input("Type your choice here: ")
    endscreen_choice=endscreen_choice.lower().strip()
    if endscreen_choice=="retry" or endscreen_choice=="r":
        m=-2
        state(m,w,b,a)
    elif endscreen_choice=="exit" or endscreen_choice=="e":
        exit()
    elif "r" in endscreen_choice:
        confirm=input("Did you mean retry? If so type R or Retry: ")
        confirm=confirm.lower().strip()
        if confirm=="r" or confirm=="retry":
            m=-2
            state(m, w, b, a)
        else:
            print("Sorry for the misunderstanding: lets try that again")
            state(m, w, b, a)
    elif "e" in endscreen_choice:
        confirm=input("Did you mean Exit? If so type E or Exit:  ")
        confirm=confirm.lower().strip()
        if confirm=="e" or confirm=="exit":
            exit()
        else:
            print("Sorry for the misunderstanding: lets try that again")
            state(m, w, b, a)
    else:
        print("That wasn't close to either of those options. Please try again.")
        state(m,w,b,a)
#starting the game
state(m, w, b, a)
    

