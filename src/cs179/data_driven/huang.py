import random


# the object of the game is the identify this ghost


class Rooms:
    def __init__(self, name, neighboors):
        """Take in a string which represents the rooms and a list of the names of connected rooms."""
        self.name = name
        self.neighbors = neighboors
        self.description = ""

    def __eq__(self, other):
        return self.name == other.name

    def get_name(self):
        return self.name

    def __str__(self):
        return self.description

    def addneighbor(self, name):
        self.neighbors.append(name)

    def change_description(self, desc):
        self.description = desc

    def is_valid_move(self, move):
        return move in self.neighbors


class House:
    def __init__(self):
        """The house class is a graph of connected rooms.
        It has multiple instance variables including: room_list and phase
        The room_list is a dictionary with keys that are the string room names corresponding to an item that is the Room representation of that room
        The phase is a 5 boolean long list used to trouble shoot the read_from_txt() method which instantiates the rooms inside of room_list along with other string values used in Game
        """
        self.room_list = {}
        self.phase = [
            False,
            False,
            False,
            False,
            False,
        ]
        self.guide, self.journal_notes, self.start_txt, self.ghostly_sign = (
            [],
            [],
            "",
            [],
        )
        self.read_from_txt()

    def get_room_list(self):
        return self.room_list

    def get_default_values(self):
        return self.guide, self.journal_notes, self.start_txt, self.ghostly_sign

    def __str__(self):
        ret = ""
        for x, y in self.room_list.items():
            ret += x + "  " + str(y.neighbors) + "\n"
        ret += "Loaded information: (rooms,descriptions,guide,journalnotes,starttxt)\n"
        ret += str(self.phase) + "\n"
        for x in self.guide:
            ret += x
        for x in self.journal_notes:
            ret += x
        ret += self.start_txt
        for x in self.ghostly_sign:
            ret += x
        return ret

    def read_from_txt(self):
        with open("huang.txt", "r") as f:
            text = f.readlines()
            chunk = []
            for line in text:
                if "END_" in line:
                    self.identify_type(chunk)
                    chunk = []
                else:
                    chunk.append(line)

    def identify_type(self, lst):
        if not self.phase[0]:
            self.phase[0] = True
            self.assign_rooms(lst)
        elif not self.phase[1]:
            self.assign_room_description(lst[0])
        elif not self.phase[2]:
            for x in lst:
                self.guide.append(x)
            self.phase[2] = True
        elif not self.phase[3]:
            for x in lst:
                self.journal_notes.append(x)
            self.phase[3] = True
        elif not self.phase[4]:
            self.start_txt = lst[0]
            self.phase[4] = True
        else:
            for x in lst:
                self.ghostly_sign.append(x)

    def assign_room_description(self, desc):
        # FIXME: some desc strings do not contain a '-'
        n = None
        if "-" in desc:
            n = desc[: desc.index("-")]
        if n is None or n == "nomorerooms":
            self.phase[1] = True
            return
        d = desc[desc.index("-") + 1:]
        # This is very sophisticated! I think you could simplify it further
        self.room_list[n].change_description(d)

    def assign_rooms(self, lst):
        for x in lst:
            x = x.strip().lower()
            roomname = x[: x.index("-")].replace(" ", "")
            self.room_list[roomname] = Rooms(roomname, x[x.index("-") + 1:].split(","))


class Ghost:
    """The ghost is an object that contains:
    the type of ghost -> str .type
    the room haunted by the ghost type Rooms -> Rooms .room
    the house haunted by the ghost -> House .house
    a list of rooms -> dictionary .room_list
    """

    ghosts = ["hantu", "obake", "poltergheist", "demon", "mimic"]

    def __init__(self):
        self.type = self.ghosts[int(random.randrange(0, len(self.ghosts)))]
        self.evidence = ""
        self.house = House()
        self.room_list = self.house.get_room_list()
        self.room = self.room_list[
            list(self.room_list.keys())[
                int(random.randrange(1, len(self.room_list.keys())))
            ]
        ]
        self.get_evi()

    def get_ghost_type(self):
        return self.type

    def get_evidence(self):
        return self.evidence

    def get_room_list(self):
        return self.room_list

    def get_house(self):
        return self.house

    def get_haunted_room(self):
        """return huanted room as a Rooms object"""
        return self.room

    def get_haunted_room_name(self):
        """return haunted room as a string"""
        return self.room.get_name()

    def has_evi(self, test):
        ret = False
        if test == "all":
            return True
        for x in self.evidence:
            if x == test:
                ret = True
        return ret

    def __str__(self) -> str:
        return self.type + "    " + self.evidence

    def reveal(self):
        print(
            "I am a "
            + self.type.upper()
            + " and you can tell me apart because of the "
            + self.evidence[0].upper()
            + " in the "
            + self.room.get_name()
        )

    def get_evi(self):
        if self.type == "hantu":
            self.evidence = ["freezing"]
        if self.type == "demon":
            self.evidence = ["emf"]
        if self.type == "poltergheist":
            self.evidence = ["examine"]
        if self.type == "mimic":
            self.evidence = ["freezing", "emf", "examine", "fingerprints"]
        if self.type == "obake":
            self.evidence = ["fingerprints"]


class Game:
    def __init__(self, easy=False, start=False):
        """The Game class represents the process of playing an entire game. After the game is over, it will quit the program.
        Parameters (easy=False,start=False)
        The first optional parameter "easy" is meant for testing, in easy mode, easy = True, the ghosts type and location will be revealed at the start.
        The second optional parameter "start" lets you decide whether or not you want to run the game right after instantiating the object. The default value false does not run the game. You can choose to manually start it after initializing the object by using the .start() method
        """
        self.current_room = "start"
        self.next = ""
        self.easy = easy
        self.spooky = Ghost()
        self.guide, self.journal_notes, self.start_txt, self.ghostly_sign = (
            self.spooky.get_house().get_default_values()
        )
        self.room_list = self.spooky.get_room_list()
        if start:
            self.start()

    def cheat(self):
        self.spooky.reveal()

    def help_menu(self):
        display = ""
        for x in self.guide:
            display += x + "\n"
        self.next = input(display)
        self.next = ""
        return self.room()

    def journal(self):
        display = ""
        for x in self.journal_notes:
            display += x + "\n"
        self.next = input(display)
        self.next = ""
        return self.room()

    def change_rooms(self, move):
        if move == "door":
            self.next = input("You shake the handle but the door is firmly locked.")
            return self.detect_cmd()
        if self.room_list[self.current_room].is_valid_move(move):
            self.current_room = move
            return self.room()
        self.next = input(
            "Invalid Move please try again or type d_Lost to get a description of the room\n"
        )
        return self.detect_cmd()

    def explore(self):
        if self.next.find("lost") != -1:
            return self.room()

        if self.spooky.get_haunted_room_name().lower() != self.current_room:
            self.next = input(
                "You look around but find nothing. What will you do next?\n"
            )
            return self.detect_cmd()

        for x in self.spooky.get_evidence():
            if x == self.next:
                if self.next == "freezing":
                    display = "Brrrrr you found freezing temperatures!"
                if self.next == "fingerprints":
                    display = "You found fingerprints in this room."
                if self.next == "emf":
                    display = "The EMF beeps loudly and reads at lvl 5."
                if self.next == "examine":
                    display = (
                        "The room is a mess! Objects seem to have been tossed around."
                    )
                self.next = input(display + " What will you do next?\n")
                return self.detect_cmd()

        self.next = input("You look around but find nothing. What will you do next?\n")
        return self.detect_cmd()

    def ghost_help(self):
        if (self.next.find("notebook") != -1) or (self.next.find("journal") != -1):
            return self.journal()
        elif self.next.find("guess") != -1:
            self.next = self.next[5:]
            if self.next == self.spooky.get_ghost_type().lower():
                print("Good work hunter! Thanks for playing!")
            else:
                print(
                    "Incorrect. The ghost knows you tried to get rid of it. Everything is dark. You run for the door but the hallway keeps getting longer. The ghost catches you.\n Better luck next time. Thanks for playing!"
                )
            quit()
        self.next = input("invalid command please try again or type help\n")
        return self.detect_cmd()

    def detect_cmd(self):
        self.next = self.next.replace(" ", "").lower()
        if self.current_room == "start":
            return self.room()

        if self.next == "help":
            return self.help_menu()
        if self.next == "quit":
            return self.exit()

        if self.next.find("moveto") != -1:
            return self.change_rooms(self.next[6:])

        if self.next.find("d_") != -1:
            self.next = self.next[2:]
            return self.explore()

        if self.next.find("guess") != -1:
            return self.ghost_help()

        self.next = input("invalid command please try again or type help\n")
        return self.detect_cmd()

    def start(self):
        if self.easy:
            self.cheat()
        return self.room()

    def room(self):
        if self.current_room == "start":
            while not (self.next == "ready" or self.next == "help"):
                self.next = (
                    input(
                        "Hello! Welcome to PhasNOphobia. Type 'Help' for help otherwise\n type 'Ready' to begin\n"
                    )
                    .lower()
                    .strip()
                )
            self.next = input(self.start_txt).lower()
            self.current_room = "hallway"

        if self.current_room == self.spooky.get_haunted_room_name():
            print(
                "\n"
                + self.ghostly_sign[int(random.randrange(0, len(self.ghostly_sign)))]
            )

        self.next = input(str(self.room_list[self.current_room]))
        return self.detect_cmd()


game = Game(False, True)
