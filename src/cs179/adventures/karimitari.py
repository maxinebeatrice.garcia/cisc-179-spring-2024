import time

wakeup = 'You come to in a dimly lit room.  A candle on a table provides the only light in the room.'
wakeupAgain = wakeup + 'The room seems familiar.  You wonder if you have been here before.'
whatToDo = 'You look around and notice you are in the center of the room, and there are four doors equidistant apart. It seems like the only way out is through one of them.'
doorMessage = 'You can pick a door to the north, south, east, or west.  Which door would you like to choose?'
doorLocked = 'The door appears to be locked.  The handle won\'t open it. There is a keyhole, so perhaps a key would unlock this door.'
pullOutKey = 'You pull out your key and decide to try it in the lock.'
keyDoesNotWork = 'This key does not fit in this lock.'
keyWorks = 'It worked! The key fits and you hear a click.'
doorStillLocked = 'The door is still locked, so maybe you should try another door.'
roomTwo = 'Upon entering, you can barely see.  Only a small candle again lights the room, but you can tell there are no other doors here.'
roomTwoWithoutKey = 'You quickly turn and go back the way you came.'
roomTwoWithKey = 'You decide to look in here again, and now you notice that a ladder is hanging from the ceiling. How strange...'
roomTwoWithKeyCont = 'Nothing ventured, nothing gained.  You climb the ladder and hit your head sooner than you expect.  Looking up, you notice a trapdoor with a keyhole.  Can you reach into your pocket for the key?'
escape = 'You swing the door upward and the smell of night hits your nose and the air rushes into your lungs.  \nYOU MADE IT OUTSIDE! \nCongratulations! Looking below, it seems like you are at the top of a castle. Now, you just have to figure out how to get down...'
roomThree = 'This room has a torch on the wall, nearly blinding you as you come inside after the dimly lit chamber you were just inside.'
roomThreeContinued = 'After your eyes adjust, you can clearly tell there is nothing of note.  As you turn to leave, you catch a glint in the corner.'
roomThreeFinish = 'You walk over and reach down to pick up a key.  Maybe this will will open a door.'
roomThreeAlreadyHaveKey = 'You already got the key from here and there is nothing else to do.  You go back.'
roomFour = 'This room is cozy.  There is a sofa and a fireplace with a lit fire.  This looks promising.'
roomFourAfterDied = 'Actually, the fireplace looks scary, and there is a creepy drapdoor.'
goBackToFirstRoom = 'This does not appear to be the way out. You go back to the first room.'
whatToDoNext = 'Which door do you want to try next? ' + doorMessage
trapDoorMessage = 'There is a trapdoor that does not appear to be locked.  Do you want to try going down there?'
trapDoorMessageYes = 'You open the hatch and descend some steep stairs.'
trapDoorMessageNo = 'You decide not to see what is down below.'
haveDiedBeforeMessage = 'On second thought, that trapdoor looks suspicious.  You should probably avoid it.'
basementMessage = 'Uh oh. You feel like this was a mistake.  Two red eyes blink open in the darkness of the room you climbed down into, and you hear a low growl.  Quick! You only have seconds, climb the stairs!'
basementDeath = 'Too late.  The monster had his jaws around you before you could even turn around.  You hear bones crunching before you lose consciousness, as a white light shines hazily in front of your eyes.'
goBackMessage = 'Do you want to go back the way you came?'
yesOrNo = 'Type y(es) or n(o) (or any word beginning with y or n will be interpreted as yes or no).'
notYesOrNo = 'You must type y(es) or n(o).'
roomChecked = 'You already checked that room. You should try another room.'
checkAgain = 'Well, you have checked all the rooms. Maybe one of the three open rooms has something you missed...'

def pause(num):
    time.sleep(num)

north = 1
south = 2
east = 3
west = 4

yes = 5
no = 6

roomsChecked = []
allRoomsChecked = False

haveDiedBefore = False
global haveKey
haveKey = False

def checkForKey():
    return haveKey


def assignDoorSelection(doorSelected):
    if (doorSelected.lower().startswith('n')):
        door = north
    elif (doorSelected[0].lower() == 'e'):
        door = east
    elif (doorSelected[0].lower() == 'w'):
        door = west
    elif (doorSelected[0].lower() == 's'):
        door = south
    else:
        door = 'You must select one of the available options.'
    return door

def assignYesOrNo(answer):
    if (answer.lower().startswith('y')):
        answer = yes
    elif (answer.lower().startswith('n')):
        answer = no
    else:
        answer = 7
    return answer

def checkAnswer(answer):
    while (answer == 7):
        print(notYesOrNo)
        answer = chooseAnswer()
    return answer

def chooseAnswer():
    response = input(yesOrNo)
    answer = assignYesOrNo(response)
    return answer

def checkDoor(door):
    while (door != 1 and door != 2 and door != 3 and door != 4):
        print(door)
        door = chooseDoor()
    return door

def chooseDoor():
    print(doorMessage)
    doorInput = input('Type n(orth), s(outh), e(ast), or w(est). (Any word starting with those four letters will be interpreted as directional.)')
    door = assignDoorSelection(doorInput)
    return door

def doorSelectionOutcome(doorSelected):
    if (doorSelected in roomsChecked):
        if (doorSelected == 1 and 3 in roomsChecked):
            print('The door is still locked, but you just found a key...')
            pause(1)
            print(pullOutKey)
            pause(1)
            print(keyDoesNotWork)
        return roomChecked
    if (doorSelected == 1):
        if (3 in roomsChecked):
            print(doorLocked)
            pause(1)
            print(pullOutKey)
            pause(1)
            print(keyDoesNotWork)
        else: 
            print(doorLocked)
    elif (doorSelected == 2):
        print(roomTwo)
        pause(1)
        print(roomTwoWithoutKey)
    elif (doorSelected == 3):
        print(roomThree)
        pause(1)
        print(roomThreeContinued)
        pause(1)
        print(roomThreeFinish)
    elif (doorSelected == 4):
        print(roomFour)
        print(trapDoorMessage)
        response = chooseAnswer()
        answer = checkAnswer(response)
        while (answer == 6):
            print('Are you sure? Maybe you should go check it out, just to be sure.  I might be the way to escape. Do you want to check it out?')
            response = chooseAnswer()
            answer = checkAnswer(response)
        if (answer == 5):
            print(trapDoorMessageYes)
            pause(2)
            print(basementMessage)
            pause(2)
            print(basementDeath)
            haveDiedBefore = True
            allRoomsChecked = False
            return haveDiedBeforeMessage
        print(trapDoorMessageNo)
    roomsChecked.append(doorSelected)
    print(len(roomsChecked))
    if (len(roomsChecked) == 4):
        allRoomsChecked = True
        haveKey = True
        return checkAgain
    return 'Now what?'

def doorSelectionAfterDied(doorSelected):
    if (doorSelected in roomsChecked):
        if (doorSelected == 1 and 3 in roomsChecked):
            print('The door is still locked, but you just found a key...')
            print(pullOutKey)
            pause(1)
            print(keyDoesNotWork)
        return roomChecked
    if (doorSelected == 1):
        if (3 in roomsChecked):
            print(doorLocked)
            print(pullOutKey)
            print(keyDoesNotWork)
        else: 
            print(doorLocked)
    elif (doorSelected == 2):
        print(roomTwo)
        pause(1)
        print(roomTwoWithoutKey)
    elif (doorSelected == 3):
        print(roomThree)
        pause(1)
        print(roomThreeContinued)
        pause(1)
        print(roomThreeFinish)
        # putKeyInPocket()
    elif (doorSelected == 4):
        print(roomFour)
        pause(1)
        print(trapDoorMessage)
        pause(1)
        print(haveDiedBeforeMessage)
        pause(1)
        print(trapDoorMessageNo)
        pause(1)
        print(goBackToFirstRoom)
    roomsChecked.append(doorSelected)
    if (len(roomsChecked) == 4):
        allRoomsChecked = True
        haveKey = True
        return checkAgain
    return 'Now what?'

def doorSelectionSecondOutcome(doorSelected):
    if (doorSelected in roomsChecked):
        return roomChecked
    if (doorSelected == 1):
        print(doorLocked)
    elif (doorSelected == 2):
        print(roomTwo)
        pause(1)
        print(roomTwoWithKey)
        pause(2)
        print(roomTwoWithKeyCont)
        pause(2)
        print(pullOutKey)
        pause(1)
        print(keyWorks)
        pause(1)
        print(escape)
        pause(5)
        return escape
    elif (doorSelected == 3):
        print(roomThree)
        pause(1)
        print(roomThreeAlreadyHaveKey)
    elif (doorSelected == 4):
        print(roomFour)
        pause(1)
        print(roomFourAfterDied)
        pause(1)
        print(goBackToFirstRoom)
    roomsChecked.append(doorSelected)
    if (len(roomsChecked) == 4):
        allRoomsChecked = True
        haveKey = True
        return checkAgain
    return 'Now what?'


print(wakeup)
pause(2)
print(whatToDo)
pause(2)
door = chooseDoor()
pause(1)
door = checkDoor(door)
pause(1)
room = doorSelectionOutcome(door)
while (room != haveDiedBeforeMessage):
    if (room == 'Now what?'):
        door = chooseDoor()
        door = checkDoor(door)
        room = doorSelectionOutcome(door)
    if (room == roomChecked):
        #print(room)
        door = chooseDoor()
        door = checkDoor(door)
        room = doorSelectionAfterDied(door)
if (room == haveDiedBeforeMessage):
    allRoomsChecked = False
    roomsChecked = []
    print(wakeup)
    pause(2)
    print(whatToDo)
    pause(2)
    door = chooseDoor()
    door = checkDoor(door)
    room = doorSelectionAfterDied(door)
# print('Already died and moving on')
door = chooseDoor()
door = checkDoor(door)
room = doorSelectionAfterDied(door)
while (room != escape and room != checkAgain):
    if (room == 'Now what?'):
        door = chooseDoor()
        door = checkDoor(door)
        room = doorSelectionAfterDied(door)
    if (room == roomChecked):
        #print(room)
        door = chooseDoor()
        door = checkDoor(door)
        room = doorSelectionAfterDied(door)
if (room == checkAgain):
    print(checkAgain)
    allRoomsChecked = False
    roomsChecked = [1] 
room = doorSelectionSecondOutcome(door)
while (room != escape and room != checkAgain):
    if (room == 'Now what?'):
        door = chooseDoor()
        door = checkDoor(door)
        room = doorSelectionSecondOutcome(door)
    if (room == roomChecked):
        #print(room)
        door = chooseDoor()
        door = checkDoor(door)
        room = doorSelectionSecondOutcome(door)
exit
