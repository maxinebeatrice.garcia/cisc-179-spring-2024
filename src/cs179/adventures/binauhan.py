print("Welcome to the text adventure!")


def start_game():
    print("You are an adventurer. One day, while roaming the lands, you discover a cave emitting beams of light towards the outside.")
    print("Your curious nature draws you inward and you enter the cavern.")

    character_class = input("You are a... 'Warrior' or 'Mage' ")

    if character_class == 'Warrior':
        print("Equipped with a sword and shield, you descend into the unknown.")
        warrior_fight()
    elif character_class == 'Mage':
        print("Equipped with a magical staff and tome of spells, you descend into the unknown.")
        mage_fight()


def warrior_fight():
    print("Upon reaching the bottom, a loud roar fills the area. You finally spot the source of the light beams.")
    print("It's a dragon, and it seems you've disturbed its slumber.")
    print("The dragon spins around, and its tail is violently lashing towards you.")

    warrior_choice1 = input("What do you do? 'Shield', 'Charge Forward', 'Run Away' ")

    if warrior_choice1 == 'Shield':
        print("Thankfully, your shield successfully blocks the attack.")
        warrior_fight_part2()
    elif warrior_choice1 == 'Charge Forward':
        print("Unable to react in time, you are struck midway through your attack. Slammed into the wall, there is unfortunately nothing you can do.")
        game_over()
    elif warrior_choice1 == 'Run Away':
        run_away()


def warrior_fight_part2():
    print("Due to your successful shield block, you now have an opportunity to make a move.")
    print("The dragon begins preparing another attack, fire emanating from its mouth.")

    warrior_choice2 = input("What do you do? 'Keep Blocking', 'Charge Forward' ")

    if warrior_choice2 == 'Keep Blocking':
        print("The dragon unleashes a breath of fire onto you. Your shield is no match for the overwhelming heat, and you are consumed by the flames.")
        game_over()
    elif warrior_choice2 == 'Charge Forward':
        print("The dragon attempts to blast you with fire, but you have the upper hand.")
        print("You are able to rush forward and triumphantly pierce the dragon's heart with your sword, putting an end to the creature.")
        good_ending()


def mage_fight():
    print("Upon reaching the bottom, a loud roar fills the area. You finally spot the source of the light beams.")
    print("It's a dragon, and it seems you've disturbed its slumber.")
    print("The dragon lunges forward at you, its sharp claws swiftly approaching.")

    mage_choice1 = input("What do you do? 'Ice Wall', 'Lightning Bolt', 'Run Away' ")

    if mage_choice1 == 'Ice Wall':
        print("The dragon's tough claws and exterior are able to easily break through your wall of ice. Game over.")
        game_over()
    elif mage_choice1 == 'Lightning Bolt':
        print("The dragon is unable to react fast enough to your spell and is struck by a bolt of lightning. Shocked by electricity, it backs away.")
        mage_fight_part2()
    elif mage_choice1 == 'Run Away':
        run_away()


def mage_fight_part2():
    print("The dragon begins preparing another attack, fire emanating from its mouth.")

    mage_choice2 = input("What do you do? 'Mana Blast', 'Fireball' ")

    if mage_choice2 == 'Mana Blast':
        print("The dragon unleashes a breath of fire onto you, however it is no match for the extreme energy coming towards it.")
        print("The dragon is overpowered by your spell, and disintegrates from the sheer amount of energy.")
        good_ending()
    elif mage_choice2 == 'Fireball':
        print("You cast fireball onto the dragon. Unfortunately, it is resistant to fire spells.")
        print("The dragon blasts you with fire, and you are consumed by its flames.")
        game_over()


def game_over():
    print("Try again?")
    start_game()


def run_away():
    print("You flee from the cave, barely escaping intact. Stricken with fear, you return to your home to live a normal life, pledging to never adventure ever again.")
    game_over()


def good_ending():
    print("Upon slaying the dragon, you notice a pile of gold and other valuable treasures surrounding the dragon's den.")
    print("You are able to return to your home with newfound wealth, and you are now known as a hero for slaying a dangerous and challenging threat to the town.")
    print("Congratulations, you've won the game!")
    quit()


start_game()
