import random

def option_over():
    over=input("Type Retry to go back to the start, or type Exit to exit the program")
    if over=="Retry":
        main()
    elif over=="Exit":
        exit()
    else:
        print("Please enter Retry or Exit")
        option_over()

def option_seven(m):
    x=random.randrange(1,100)
    print("Its game 7 of the World Series. The whole season rests on this game and this game alone. Fortunately your ace is on the mound and has silenced the opposing offense. Entering the 7th inning they are set to face the best three opposing hitters. You know that batters get significantly better, adding 70 points of OPS, when seeing a pitcher for the third time in a game. On the other hand, your ace has been pitching very well, why break that up?")
    seven=input("Enter Sub to take your ace out of the game or Stay to keep him in: ")
    if seven=="Sub":
        if x+m>80:
            print("Your bullpen continues this dominant show of pitching, and your offense comes alive to secure the trophy! Congratulations on becoming World Series Champions!")
            option_over()
        else:
            print("A big 8th inning rally gives the other team the lead. Unfortunately you have come just short of lifting the trophy.")
            option_over()
    elif seven=="Stay":
        if x+m>30:
            print("Your ace's performance in this year's playoffs will go down in the history books as one of the greatest of all time. Congratulations on winning the World Series and getting to lift that trophy!")
            option_over()
        else:
            print("Though heroic, your ace's attemps are in the end futile as with the massive workload they have put on their arm it was bound to give up at some point. You have come just short of lifting the trophy.")
            option_over()
    else:
        print("Please enter either Sub or Stay")
        option_seven(m)

def option_six(m):
    x=random.randrange(1,100)
    print("Just two series from lifting the World Series trophy! Unfortunately you are already down two games in a best of 5. You need to win the next three to advance. You could stay with the statistically optimal order you have been using all season, or you could shuffle the order to try to jumpstart you offense.")
    six=input("Enter Stay or Shuffle to make your choice: ")
    if six=="Stay":
        if x+m>75:
            print("Your team comes in clutch and is able to win the next three games to make it to the semi-finals, which you win easliy. You are World Series bound!")
            option_seven(m)
        else:
            print("Your team isn't able to turn it around and you have been knocked out of the playoffs.")
            option_over()
    elif six=="Shuffle":
        if x+m>30:
            print("The new order jumpstarts your offense, which secures you the series win. You sweep the semi-final round and are World Series bound!")
            option_seven(m)
        else:
            print("Nothing seems to be able to restart your offense and you have been knocked out of the playoffs.")
            option_over()
    else:
        print("Please make sure you enter either Stay or Shuffle")
        option_six(m)
    
def option_five(m):
    x=random.randrange(1,101)
    print("Welcome to the playoffs! Your team dominates game one behind 8 innings without a run from your Ace starting pitcher, and a 1-2-3 9th from your closer. However game two goes the other way due to your relief pitchers poor showing out of the bullpen. Its the 6th inning out of 9 in game three, and your team is holding on to a one run lead. However your starting pitcher is running out of steam. Do you trust your relief pitchers to close the game out or do you want to bring your ace bback out on one day of rest to try to make it to the 9th inning so only your closer has to pitch?")
    five=input("Make your selection by typing Bullpen or Starter: ")
    if five=="Bullpen":
        if x+m>50:
            print("Your bullpen pulls through. You have made it to the next round!")
            option_six(m)
        else:
            print("Your bubllpen wasn't able to see the game out. You have been knocked out of the playoffs.")
            option_over()
    elif five=="Starter":
        if x+m>25:
            print("Your ace is having the playoff performance of a lifetime. The lead is held and you have made it to the next round!")
            option_six(m)
        else:
            print("Especially with their heavy workload just two days ago your ace is unable to keep the other team at bay. You have been knocked out.")
            option_over()
    else:
        print("Please make sure you enter either Bullpen or Starter")
        option_five(m)

def option_four(m,w):
    x=random.randrange(1,101)
    print("Disaster strikes! Your star 3rd baseman has sustained a season ending injury. There are two players on your minor league team who can replace him. The first is Marv Grissom. In the previous month Marv changed his hitting mindset for minor gains and is swinging at strikes 65% of the time up from 62%, while reducing his swings at balls to 25% from 30%. Meanwhile Mike Bacsik is on a hot streak of his own, batting .389 in the last month.")
    four=input("Make your selection by typing Grissom or Bacsik: ")
    if four=="Grissom":
        if m+x>15:
            w=w+1
            if w==4:
                print("Marv Grissom is able to translate better mindset to better results. Your team ends the season with a record of 101-61. This secures you a first round bye!")
                option_six(m)
            elif w==3:
                print("Marv Grissom is able to translate better mindset to better results. Your team ends the season with a record of 91-71. You have made it to the playoffs!")
                option_five(m)
            elif w==2:
                print("Marv Grissom is able to translate better mindset to better results. Your team ends the season with a record of 86-76. You have made it to the playoffs!")
                option_five(m)
            else:
                print("Marv Grissom is able to translate better mindset to better results. Your team ends the season with a record of 79-83. You have unfortunately missed the playoffs.")
                option_over()
        else:
            if w==3:
                print("Marv Grissom is unable to replace lost production. Your team ends the season with a record of 91-71. You have made it to the playoffs!")
                option_five(m)
            elif w==2:
                print("Marv Grissom is unable to replace lost production. Your team ends the season with a record of 86-76. You have made it to the playoffs!")
                option_five(m)
            elif w==1:
                print("Marv Grissom is unable to replace lost production. Your team ends the season with a record of 79-83. You have unfortunately missed the playoffs.")
                option_over()
            else:
                print("Marv Grissom is unable to replace lost production. Your team ends the season with a record of 71-91. You have unfortunately missed the playoffs.")
                option_over()
            
    elif four=="Bacsik":
        if m+x>50:
            if w==4:
                print("Mike Bacsik's hot streak continues. Your team ends the season with a record of 101-61. This secures you a first round bye!")
                option_six(m)
            elif w==3:
                print("Mike Bacsik's hot streak continues. Your team ends the season with a record of 91-71. You have made it to the playoffs!")
                option_five(m)
            elif w==2:
                print("Mike Bacsik's hot streak continues. Your team ends the season with a record of 86-76. You have made it to the playoffs!")
                option_five(m)
            else:
                print("Mike Bacsik's hot streak continues. Your team ends the season with a record of 79-83. You have unfortunately missed the playoffs.")
                option_over()
        else:
            if w==3:
                print("Mike Bacsik's hot streak ends abruptly. Your team ends the season with a record of 91-71. You have made it to the playoffs!")
                option_five(m)
            elif w==2:
                print("Mike Bacsik's hot streak ends abruptly. Your team ends the season with a record of 86-76. You have made it to the playoffs!")
                option_five(m)
            elif w==1:
                print("Mike Bacsik's hot streak ends abruptly. Your team ends the season with a record of 79-83. You have unfortunately missed the playoffs.")
                option_over()
            else:
                print("Mike Bacsik's hot streak ends abruptly. Your team ends the season with a record of 71-91. You have unfortunately missed the playoffs.")
                option_over()
    else:
        print("Make sure you type either Grissom or Bacsik")
        option_four(m, w)

def option_three(m,w,a):
    x=random.randrange(1,101)
    print("It is nearing the deadline to make trades and two teams are offering you star players to help you make a push for the playoffs. Option one is the 40 year old DH, meaning he doesn't play defense, Terry Cox. Cox has won the Silver Slugger, the award for best hitter at a position, two years consecutively as well as coming top 10 in MVP voting. His OPS+ for this year is 159. A different team is also offering you a DH. They propose you trade for 29 year old Ferris Fain. Fain is two years removed from an All-Star selection and a Silver Slugger, and has put up an adjusted OPS+ of 165 this year.")
    three=input("Please enter Cox or Fain to make your selection: ")
    if three=="Fain":
        #if Fain is traded for
        if x+m>25:
            w=w+1
            if w==3:
                print("Fain continues his stellar season and will get votes for MVP after the postseason. Your team continues its good form reaching a record of 74-46.")
            elif w==2:
                print("Fain continues his stellar season and will get votes for MVP after the postseason. Your team's record is now 67-63.")
            else:
                print("Fain continues his stellar season and will get votes for MVP after the postseason. Your team's record is now 61-59.")
            option_four(m, w)
        else:
            if w==2:
                print("Fain is unable to recapture his first half success. Your team is now 67-63.")
            elif w==1:
                print("Fain is unable to recapture his first half success. Your team is now 61-59.")
            else:
                print("Fain is unable to recapture his first half success. Your team is now 50-70.")
            option_four(m, w)
    elif three=="Cox":
        #if Cox is traded for
        if x+m>75:
            w=w+1
            if w==3:
                print("Cox continues his stellar season and will get votes for MVP after the postseason. Your team continues its good form reaching a record of 74-46.")
            elif w==2:
                print("Cox continues his stellar season and will get votes for MVP after the postseason. Your team's record is now 67-63.")
            else:
                print("Cox continues his stellar season and will get votes for MVP after the postseason. Your team's record is now 61-59.")
            option_four(m, w)
        else:
            if w==2:
                print("Cox is unable to recapture his first half success. Your team is now 67-63.")
            elif w==1:
                print("Cox is unable to recapture his first half success. Your team is now 61-59.")
            else:
                print("Cox is unable to recapture his first half success. Your team is now 50-70.")
            option_four(m, w)
    elif three=="Both":
        #secret option
        if a==0:
            w=w+1
            if w==3:
                print("You trade away top prospect Christian Walker to get both players. Your team is now 74-46.")
            elif w==2:
                print("You trade away top prospect Christian Walker to get both players. Your team is now 67-63.")
            else:
                print("You trade away top prospect Christian Walker to get both players. Your team is now 61-59.")
            option_four(m,w)
        else:
            print("You do not have the prospect capital to trade for both players")
            option_three(m, w, a)
    else:
        print("Make sure you enter Fain or Cox")
        option_three(m, w, a)
            

def option_two(m,w):
    # second choice
    x=random.randrange(1,101)
    print("One of your top prospects, Christian Walker is hitting really well in your minor league team, with an ops of 1.218 which is extremely high. Your coaches are asking you if now is the right time to promote him to the Majors.")
    two=input("Enter Yes to promote Walker to your team, or No to let him develop for the future: ")
    if two =="Yes":
        #if Walker is promoted
        a=1
        if x+m>20:
            w=w+1
            if w==2:
                print("Walker has an immediate impact and is being hailed as a generational player. Your team surges to 48-32")
            else:
                print("Walker has an immediate impact and is being hailed as a generational player. Your team surges to 41-39")
            option_three(m,w,a)
        else:
            if w==1:
                print("Walker isn't able to significantly contribute to your team, which now has a record of 41-39")
            else:
                print("Walker isn't able to significantly contribute to your team, which now has a record of 35-45")
            option_three(m,w,a)
    elif two =="No":
        #if walker stays in the minors
        a=0
        if w==1:
            print("Without Walker your team reaches a 41-39 record after 80 games")
        else:
            print("Without Walker your team's record is 35-45")
        option_three(m,w,a)
    else:
        print("Make sure your answer is Yes or No")
        option_two(m, w)

def option_one(m,w):
    # first choice
    x=random.randrange(1,101)
    print("Welcome to spring training! Its a new year of baseball. The first choice you have to make is which of two veterans to give a chance to join your team. First is Shane Reynolds, a speedy outfielder who has a pentient for getting hit by pitches. Last year he put up a .250 batting average, a .357 on base percentage and a .340 slugging for an adjusted OPS of 84. The other option is Candy Sierra, who is not as good defensively but hits significantly more home runs. His slashline was .269/.299/.413 for a comparable OPS+ of 82.")
    one=input("Type Shane or Candy to make your choice: ")
    if one =="Shane":
        #if Shane Reynolds is taken
        if x+m>40:
            #60% chance of win
            w=w+1
            print("Shane has a breakout year helping your team get out to a 24-16 start")
            option_two(m,w)
        else:
            print("Shane is unable to break into your team, and your team gets off to a slow start of 17-23")
            option_two(m,w)
    elif one == "Candy":
        #if Candy Sierra is taken
        if x+m>80:
            #20% chance of win
            w=w+1
            print("Candy has a breakout year helping your team get out to a 24-16 start")
            option_two(m,w)
        else:
            print("Candy is unable to break into your team, and your team gets off to a slow start of 17-23")
            option_two(m,w)
    else:
        print("Please make sure your answer is either Shane or Candy")
        option_one(m,w)
        
def tutorial(w,m):
    # Code for tutorial
    print("In this game you will be offered choices that will have an impact on the season of your team. These choices will have a 'right' and 'wrong' answer, but there is some randomness in the result that will happen.")
    print("For your first choice you have to hire a manager for your team. The first option is promoting Wiley Griggs, who has served as the teams bench coach, like a second in command, or to hire Miguel Mejia, a ex-player looking for their first job after retiring")
    manager=input("Simply enter Griggs or Mejia to make your choice: ")
    if manager == "Griggs":
        # if Wiley Griggs is chosen
        m=5
        print("Going with someone already in the organization helps keep the clubhouse consistent and happy. Gain a 5% boost to all future choices.")
        option_one(m,w)
    elif manager == "Mejia":
        # if Miguel Mejia is chosen
        print("Miguel certainly isn't a bad manager, but the quick change in style from last year means you don't gain any boost to future choices.")
        option_one(m,w)
    else:
        print("Please make sure your answer is either Griggs or Miguel")
        manager=input()
        if manager == "Griggs":
             # if Wiley Griggs is chosen
             m=5
             print("Going with someone already in the organization helps keep the clubhouse consistent and happy. Gain a 5% boost to all future choices.")
             option_one(m,w)
        elif manager == "Miguel":
             # if Miguel Mejia is chosen
             print("Miguel certainly isn't a bad manager, but the quick change in style from last year means you don't gain any boost to future choices.")
             option_one(m,w)
          
def main():
    m=0
    w=0 
    # intro
    print("Welcome to Inside the Park, a game in which you get to be the GM of a baseball team for a year. The goal is to win the World Series this year.")
    # Get user input
    t=input("Would you like a tutorial. If yes enter Yes and if not enter No: ")
    # Option handling
    if t == "Yes":
        tutorial(m,w)
    elif t == "No":
        # Code for option two
        option_one(m,w)
    else:
        print("You didn't enter either Yes or No, please try again")
        main()
if __name__ == "__main__":
    main()