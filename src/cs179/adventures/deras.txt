
You awaken with a gasp. Coughing, you shield your eyes from the blazing sun. Without moving, you search your mind for any memories, any recollection of how you got there. Where am I? WHO am I? But you come up empty. Pushing yourself into a sitting position, you blink until your eyes have adjusted to the view in front of you. Spread out before you are white sands, and beyond that, crashing blue waves, a scene that would be picturesque in any other circumstance. Behind you stretches a dark, lush tropical forest. You are sore, sunburnt, and confused, but you have to make a decision. The sun is high in the sky, and if you want to survive, you need to act fast. You search your clothing and find a pack of waterproof matches but nothing more.

---
I assumed you were braver than that. My mistake.
****************************************************
---
You feel safer in the open. You decide to walk the waterline and see if you can find anything that increases your chances of survival. You can also see some cliffs in the distance. 
Walking parallel to the waterline, you shield your eyes and scan the area in front of you. You walk for a few miles, and the cliffs in the distance come closer. They are starting to veer away from the beach, so you are conflicted on which route to take.

---
You decide to continue along the water. Suddenly, you come across what appears to be a mostly intact boat floating a few yards out in the water.

---
You decide to swim out to the boat despite your weakened state. Fighting the current, you slowly but surely get closer to the craft. Gripping the edge, you look into the boat. With a gargantuan effort you heave yourself over the side. The boat rocks frantically side to side and you lay as still as possible, hoping it doesn’t upset. 

After a few seconds that feel like hours, the boat’s rocking steadies to match the current. You sit up in exultation only to be met with a horrifying view. The force of you climbing into the boat must have dislodged whatever was holding it close to the beach, and you are now caught in a riptide. Unable to paddle and return to the beach, you know you are too exhausted to swim back. As the island fades on the horizon, along with it fades your hope of survival. You die lost at sea. 1/12
****************************************************
---
You may be able to summon enough strength to get to the boat, but being able to come back is another story. You decide that you will wait and store energy before trying to swim to the boat. You continue traveling up the beach, and after a few hours, you see smoke rising in the air. 

At first believing it to be a trick of the dying light, you continue on unsteadily. Getting closer, you see that it’s a campfire, and sitting next to it is a woman in tattered clothing. You approach cautiously, but once you both realize the other isn’t a threat, you begin talking. You find out that she is also a survivor of the mysterious shipwreck you were a victim of, and neither of you remember how you got there. Luckily, the stranger has a survival backpack with food, a water purifying system and other survival items. You join forces and together you are able to stay alive until a search and rescue party finds you a few days later. 2/12
****************************************************
---
You see the cliffs in the distance as potential shelter or a possible vantage point, so you decide to investigate them. After a few hours, of walking, you have reached the cliff face. You observe that there is a steep path almost hidden by brush that would allow you to climb up to the top of the cliffs, but they also curve out of view on the beach, so you may find something more by staying at ground level.

---
You decide to climb the cliff face. You begin the
treacherous climb with sweaty palms, and lose your footing a few times. You are beginning to regret your decision, and have almost decided to climb back down, but you are able to pull yourself onto a ledge and sit up, looking out to sea as you catch your breath. 

Amazingly, you see a ship in the distance! Renewed with hope, you look up and realize you are almost at the top of the cliffs. You quickly complete the rest of the climb. At the top, you quickly gather tinder and start a signal fire. At first the ship appears to continue its path, but then you see it begin to turn toward you, and it quickly makes its way to the beach. You’re saved by a passing cruise ship! 3/12
****************************************************
---
You choose not to risk your safety climbing the unstable cliff face. You continue on your walk along the beach, and a few hours later as it begins to curve, you round the cliffs to see a cove. Just in time, as storm clouds paint the sky, a bolt of lightning lights up the mouth of a cave at the bottom of the cliffs. A sudden thunderous downpour drives you to quickly seek shelter. 

Once inside, you strike a match on the wall to look around. It seems safe enough and you are exhausted, so you decide to rest. Sitting down against the rock wall, you nod off to the sound of the thunder outside. Unfortunately, the thunderstorm combined with high tide results in a flash flood sealing the entrance of the cave. You awaken as water pours into the cavern around you. Horrified, hope fades as the water level quickly rises. This apparent shelter becomes your tomb. 4/12
****************************************************
---
You turn and face the jungle. Stepping closer, the cool air that emanates from the shade beckons. The plant life is dense, allowing very little light to touch the ground under the trees, but if you’re trying to
find food or fresh water, you believe the jungle is your best bet.

---
You travel directly into the jungle, trying to maintain as straight of a route as possible. It’s difficult because of how dense the undergrowth is, and after a few hours of travel you start to worry that you’ve lost your way. Just as you are about to turn back and try to retrace your steps, you stumble upon a clearing. Within are the remains of a fire and some other signs of human life. The fire is still smoldering, so whoever was there has recently left.

---
You decide to retreat back into the foliage and observe for a few moments. Faster than you expected, you hear steps approaching. Looking into the clearing, you see a group of three people walk into view. They appear to be natives and are speaking to each other in a language you don’t understand. 

Apprehensive but hopeful, you stand, raise your hands in appeasement, and step out in front of them. They react in surprise and defensiveness, but quickly realize you are not a threat. Initially wary, they welcome you into their island
community, teaching you survival skills and absorbing you into their tribe. You live out the rest of your days as valued member of the group. 5/12
****************************************************
---
Fearing the unknown, you hesitate to enter the clearing. You freeze when you sense movement getting closer. You hear rustling in the brush across the way, and fearing a predator or unfriendly humans, you turn and flee. You run frantically and with no direction, and quickly become even more disoriented. Stumbling deep into the jungle’s labyrinth with no resources, you succumb to the wilds. 6/12
****************************************************
---

You decide to hug the forest’s edge, scared to lose sight of the ocean. After a few hours, you come across what appears to be a well-worn path entering the forest. You decide to follow the path finding comfort in having a route that would allow you to retrace your steps. The path leads you to the remains of a mysterious temple. The stone floors and walls are covered with vines, with painting and carvings peaking through.

---
Driven by curiosity, you decide to inspect the ruins further. Brushing aside some vines, you accidentally dislodge a large, oddly shaped stone set in the wall. As it falls to the ground, a quiet rumbling begins beneath you. It gets louder, and the earth beneath you begins to crumble. Horrified, you attempt to grip the vines around you, but they slip from your hands. The ground collapses under your feet, and you fall to your death in a hidden pit trap. 7/12
****************************************************
---
Feeling trepidation, you slowly back away from the structure. Just in time, too, because as you exit, a loud rumbling begins and a rockfall closes off the temple completely. 

Following the path back toward the beach, you see something you must have missed earlier: a barely visible trail veers off east from the main path. In a last ditch effort, you decide to follow the trail, and it leads you to an abandoned hut. Inside you find a first aid kit, food, water, and a guidebook to local flora and fauna. Armed with these vital supplies, you are able to stay alive until your signal fire is seen by a passing research plane weeks later. 8/12
****************************************************
---
Exhausted and sore, you decide that the best course of action is to attempt to rest and recuperate. You will do what you can to survive, but you don't want to leave the area in the off chance someone comes looking for you. You decide to search the wreckage surrounding you, and come across a limited amount of resources: a canteen of fresh water, a multi-tool, flares, and a can of food.

---
You don’t want to count on someone rescuing you, so you decide to conserve what you have. You do know that you need shelter, but you are also extremely dehydrated.

---
You’re very thirsty, but you think that you’ll be able to retain more water if you have shelter to shield you from the sun. You decide to gather the wreckage around you and do your best to piece together
some sort of cover. It’s exhausting work as some of the pieces are heavy and large, but you are able to gather quite a bit of material. 

This takes a few hours, and as you are just about to complete the third side of your shelter, you slip on a piece of wood and your foot twists painfully. You drop to the ground, clutching your ankle in your hands. Gingerly, you attempt to rotate the joint, but pain shoots up your leg. Panic sets in as you realize it’s nightfall, you don’t have adequate shelter or water, and you are now injured. You were already in a weakened state, so this is the nail in the coffin for you. You make it a few more hours before finally succumbing to exposure to the elements. 9/12
****************************************************
---
It's so hot, and nightfall is looming, but you don’t think you’ll make it very far without more water. Expanding your search to the more scattered boat remains, you come across a small crate. Using the multi-tool, you pry it open and inside you find cooking supplies, including a pot. You are ecstatic, because you know with a pot you’ll have the ability to boil seawater to make it drinkable. With access to unlimited fresh water, you build a camp and are able to stay alive until a passing fishing boat sees you a few days later. You are rescued! 10/12
****************************************************
---
You don’t want to expend unnecessary energy, after all you should be rescued soon. You don’t know how you got here, but surely someone is looking for you and its only a matter of time until they find you.

---
You glance around you trying to take inventory of any possible supplies. You have some wood and a few matches, and you eat the can of food right away to keep your energy up. But you don’t want to create a fire unless there is someone to see it in an effort to conserve firewood. You sink to the ground, eyes locked on the horizon. As the sun begins to go down, along with it goes your hope and desire to survive. The despair becomes so overwhelming that you curl up on the sand and resign yourself to your fate. You lacked the will to survive, and for that you paid the ultimate price. 11/12
****************************************************
---
You don’t want to risk not having a signal fire lit in the off chance a ship passes by, so you gather the wood around you. Initially you start off with a small fire, not wanting to run out if no one comes. You waste a few matches trying to get the fire started, and your heart begins to pound as your finite resources dwindle.

 You try to ration the wood to make it last, but after a few hours of the fire burning and no rescue in sight, you begin to panic. The sun is getting lower in the sky, and just when you think that you’re condemned to death, you hear a buzzing sound in the sky. Looking up, you see a small bush plane approaching and you jump to your feet, waving your arms frantically. The plane quickly flies toward you, circling overhead to let you know you’ve been seen. You’re saved! 12/12
****************************************************
---
Invalid choice. You lose!
****************************************************
---
The beach stretches out in front of you. Behind you the jungle foliage is dark and thick, but the shade is inviting to your sunburnt skin. What will you do first? Search the (a) beach, explore the (b) jungle or (c) stay where you are?
****************************************************
---
Will you continue along the (a) waterline, or head toward the (b) cliffs?
****************************************************
---
Do you go out into the water to (a) investigate, or (b) keep walking?
****************************************************
---
What do you choose, (a) climb the cliff path or (b) stay at ground level?
****************************************************
---
You have to decide which route to take, are you going to (a) head straight into forest from where you’re at, or (b) walk along the outskirts?
****************************************************
---
Do you (a) wait and see if someone comes back, or (b) run away before they return?
****************************************************
---
Do you (inspect) the ruins more thoroughly, or (leave)?
****************************************************
---
Do you (a) build a shelter first, or (b) try to find more water?
****************************************************
---
Do you decide to (a) ration your meager supplies to extend the length they will keep you alive, or (b) use the resources up and hope you are rescued before they run out?
****************************************************
---
In the mean time, do you (a) build a signal fire, or (b) monitor the horizon in the hopes you will see a ship?
****************************************************
---           





